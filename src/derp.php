<?php
require __DIR__ . '/../vendor/autoload.php';

$spoon = new \Spoon\Spoon;
$done = false;

// wait on child processes when they complete
$spoon->signal(SIGCHLD, function () use ($spoon) {
    $pid = $spoon->wait();
    echo "Released {$pid}\n";
});

$spoon->signal(SIGINT, function () use ($spoon, &$done) {
    echo "Interrupt caught, cleaning up child processes..\n";
    $spoon->waitAll();
    $done = true;
});

while (!$done) {
    $spoon->fork(function () {
        $pid = getmypid();
        echo "[{$pid}]: hello world\n";
        sleep(25);
        echo "[{$pid}]: all done!\n";
    });
    $spoon->dispatch();
    sleep(10);
}

