<?php

namespace Sock;

use Sock\Exception\SockAcceptException;
use Sock\Exception\SockCreateException;
use Sock\Exception\SockDestroyException;

/**
 * Class Sock: unix socket server wrapper
 * @package Sock
 */
class Sock
{
    protected $path;
    protected $socket;
    protected $timeout;

    public function __construct($path, $timeout = 1)
    {
        $this->path = $path;
        $this->socket = null;
        $this->timeout = $timeout;
    }

    public function open()
    {
        if ($this->socket !== null) {
            throw new SockCreateException('Socket server is already open');
        }

        if (file_exists($this->path)) {
            throw new SockCreateException('Socket exists. Is another instance running?');
        }

        $this->socket = @stream_socket_server("unix://{$this->path}");

        if (!$this->socket) {
            throw new SockCreateException('Unable to create socket server');
        }

        if(!stream_set_timeout($this->socket, $this->timeout)) {
            throw new SockCreateException('Unable to set socket timing');
        }
    }

    public function close()
    {
        if ($this->socket === null) {
            throw new SockDestroyException('Socket does not need to be closed');
        }

        if (!fclose($this->socket)) {
            throw new SockDestroyException('Could not close socket');
        }

        if (!unlink($this->path)) {
            throw new SockDestroyException('Could not remove socket');
        }

        $this->socket = null;
    }

    public function accept()
    {
        if ($this->socket === null || !is_resource($this->socket)) {
            throw new SockAcceptException('Socket must be open before accepting connections');
        }

        return new SockClient(@stream_socket_accept($this->socket));
    }
}