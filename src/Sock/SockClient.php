<?php

namespace Sock;
use Sock\Exception\SockClientCreateException;
use Sock\Exception\SockClientWriteException;

/**
 * Class SockClient: client for interacting with socket server
 * @package Sock
 */
class SockClient
{
    protected $connection;

    /**
     * SockClient constructor.
     * @param $connection
     * @throws SockClientCreateException
     */
    public function __construct($connection)
    {
        if (!is_resource($connection) && !is_string($connection)) {
            throw new SockClientCreateException('Invalid connection parameter');
        }

        if (is_string($connection)) {
            $client = @stream_socket_client("unix://{$connection}");
            if (!$client) {
                throw new SockClientCreateException('Unable to connect to socket');
            }
            $this->connection = $client;
        } else {
            $this->connection = $connection;
        }
    }

    /** writes a message to the socket
     * @param $message
     * @return int
     * @throws SockClientWriteException
     */
    public function write($message)
    {
        $bytes = @fwrite($this->connection, $message);
        if ($bytes === 0 && strlen($message) > 0) {
            throw new SockClientWriteException('Remote connection closed');
        }

        return $bytes;
    }

    public function read()
    {
        while ($line = fgets($this->connection)) {
            echo $line;
        }
    }
}