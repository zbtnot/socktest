<?php

namespace Sock\Exception;

/**
 * Class SockDestroyException: for issues with socket server destruction
 * @package Sock\Exception
 */
class SockDestroyException extends \Exception
{
    public function __construct($message, $code = 1, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}