<?php

namespace Sock\Exception;


class SockClientWriteException extends \Exception
{
    public function __construct($message, $code = 3, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}