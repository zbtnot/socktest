<?php

namespace Sock\Exception;


class SockClientCreateException extends \Exception
{
    public function __construct($message, $code = 4, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}