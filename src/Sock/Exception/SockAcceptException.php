<?php

namespace Sock\Exception;


class SockAcceptException extends \Exception
{
    public function __construct($message, $code = 2, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}