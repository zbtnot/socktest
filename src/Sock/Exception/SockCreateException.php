<?php

namespace Sock\Exception;

/**
 * Class SockCreateException: for issues with socket server creation
 * @package Sock\Exception
 */
class SockCreateException extends \Exception
{
    public function __construct($message, $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}