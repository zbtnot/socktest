<?php

namespace Spoon;

/**
 * Class Spoon: A simple process management class
 * @package Spoon
 */

class Spoon
{
    protected $parentPid;
    protected $children = [];

    public function __construct()
    {
        $this->parentPid = getmypid();
    }

    /** forks the process and if we are in the child process, executes $method
     * @param callable $method
     * @return int the process id returned from forking
     */
    public function fork(callable $method)
    {
        $pid = pcntl_fork();
        switch ($pid) {
            case -1:
                throw new \RuntimeException('Unable to fork the process.');
                break;
            case 0:
                $method();
                die; // kill the fork after the closure completes
                break;
            default:
                $this->children[$pid] = $pid;
                break;
        }

        return $pid;
    }

    /** wrapper around pcntl_waitpid -> waits on the process $pid if it belongs to our child process pool
     * only allowed from the parent process
     * @param $pid
     * @return null|int the id of the process waited on, or null if failure
     */
    public function wait($pid = -1)
    {
        if ($this->parentPid !== getmypid()) {
            return null;
        }

        $pid = pcntl_waitpid($pid, $status);
        if (isset($this->children[$pid])) {
            unset($this->children[$pid]);
        }

        return $pid;
    }

    /** like Spoon::wait, except this waits on all pids in Spoon::children
     * @return void
     */
    public function waitAll()
    {
        if ($this->parentPid !== getmypid()) {
            return;
        }

        foreach ($this->children as $child) {
            $this->wait($child);
        }
    }

    /** wrapper around pcntl_signal -> maps the global signal handler for the signal $type
     *  to $method
     * @param $type
     * @param callable $method
     */
    public function signal($type, callable $method)
    {
        pcntl_signal($type, function ($signalCode) use ($method) {
            $method($signalCode);
        });
    }

    /** wrapper around pcntl_signal_dispatch for event calls
     * this should be called from your parent process's main loop
     */
    public function dispatch()
    {
        pcntl_signal_dispatch();
    }
}
