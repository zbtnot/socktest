<?php
require __DIR__ . '/../vendor/autoload.php';

$sockPath = __DIR__ . '/derp2.sock';
$sock = new \Sock\Sock($sockPath);
$spoon = new \Spoon\Spoon;
$done = false;

// wait on child processes when they complete
$spoon->signal(SIGCHLD, function () use ($spoon) {
    $pid = $spoon->wait();
    echo "Released {$pid}\n";
});

$spoon->signal(SIGINT, function () use ($spoon, &$done) {
    echo "Interrupt caught, cleaning up child processes..\n";
    $spoon->waitAll();
    $done = true;
});

$sock->open();

echo "Waiting for connection..\n";
while (!$done) {
    $spoon->dispatch();
    try {
        $client = $sock->accept();
    } catch (\Sock\Exception\SockClientCreateException $e) {
        continue;
    }

    var_dump($client);
    $spoon->fork(function () use ($client) {
        echo "connected!\n";
        $pid = getmypid();
        while (true) {
            try {
                $data = "{$pid}: " . date('Y-m-d H:i:s') . "\n";
                $client->write($data);
                sleep(1);
            } catch (\Sock\Exception\SockClientWriteException $e) {
                echo "{$pid} connection closed\n";
                break;
            }
        }
    });
}

$sock->close();
echo "Bye!\n";